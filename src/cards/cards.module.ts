import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { WsModule } from 'src/ws/ws.module';
import { AppService } from 'src/app.service';

@Module({
  imports: [WsModule,HttpModule],
  controllers: [CardsController],
  providers: [CardsService, AppService]
})

export class CardsModule {}
