import { Body, Controller, Post, Req, Res } from '@nestjs/common';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { UuidDto } from 'src/dto';
import { CardsService } from './cards.service';
import { monitoringDto } from './dto';

@Controller('cards')
@ApiTags('Cards')
export class CardsController {
    constructor(private service: CardsService){}

    @Post('/getuserallcards')
    @ApiHeader({ name: 'Accept-Language', description: 'Default ru' })
    async getUserallCards(@Body() body: UuidDto, @Res() res: Response, @Req() req: Request) {
        return await this.service.getUserallCards(body, res, req)
    }
    
    @Post('/monitoring')
    @ApiHeader({ name: 'Accept-Language', description: 'Default ru' })
    async monitoring(@Body() body: monitoringDto, @Res() res: Response, @Req() req: Request) {
        return await this.service.monitoring(body, res, req)
    }
}
