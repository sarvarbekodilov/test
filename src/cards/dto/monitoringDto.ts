import { IsString } from 'class-validator'
import { ApiProperty } from "@nestjs/swagger"; 

export class monitoringDto {
    @IsString() 
    @ApiProperty({type: String, default:'99eae575-12ae-48bb-8470-fd9e214b004e', description: 'uuid'})
    uuid: string
    
    @IsString() 
    @ApiProperty({type: String, default:'a01340ec-0f67-48e8-8393-3944a4b82a09', description: 'Card uuid'})
    card_uuid: string
    
    @IsString() 
    @ApiProperty({type: String, default:'D', description: 'Monitoring Type'})
    monitoringType: string
    
    @IsString() 
    @ApiProperty({type: String, default:'10', description: 'Month'})
    month: string

    @IsString() 
    @ApiProperty({type: String, default:'2022', description: 'Year'})
    year: string
    
}