import { Injectable } from '@nestjs/common';
import { Request, Response } from 'express';
import { appendFileSync, existsSync, mkdirSync } from 'fs';
import { join } from 'path';

@Injectable()
export class AppService {
  log(req: Request, request_id: string, response){
      let d = new Date()
      let date = new Date(d).toLocaleString('uz-UZ')
      let year = d.getFullYear()
      let month = d.getMonth()
      let day = d.getDay().toString().padStart(2,'0')
      let hour = d.getHours() 
      let today = d.getDate()

      let path = req.route.path 
      let method = req.method
      let ip = req.ip
      let userAgent = req.headers['user-agent']
      let body = JSON.stringify(req.body)

      if(!existsSync(`logs/logs/${year}/${month}/${day}`)){
          mkdirSync(`logs/logs/${year}/${month}/${day}`, {recursive: true});
      }
      appendFileSync(join(process.cwd(),`logs/logs/${year}/${month}/${day}/${hour}_${today}.log`), date+' RequestId: '+ request_id +' IP: '+ip+' Path: '+path+' Method: '+method+' Body: '+body+' Device: '+userAgent+'---> Response '+JSON.stringify(response)+'\n' )
  }

  errorlog(req: Request, error, request_id:string){
      let d = new Date()
      let date = new Date(d).toLocaleString('uz-UZ')
      let year = d.getFullYear()
      let month = d.getMonth()
      let day = d.getDay().toString().padStart(2,'0')
      let hour = d.getHours() 
      let today = d.getDate()

      let path = req.route.path 
      let method = req.method
      let ip = req.ip
      let userAgent = req.headers['user-agent']
      let body = JSON.stringify(req.body)

      if(!existsSync(`logs/errorlogs/${year}/${month}/${day}`)){
          mkdirSync(`logs/errorlogs/${year}/${month}/${day}`, {recursive: true});
      }

      appendFileSync(join(process.cwd(),`logs/errorlogs/${year}/${month}/${day}/${hour}_${today}.log`), date+' RequestId: '+ request_id +' IP: '+ip+' Path: '+path+' Method: '+method+' Body: '+body+' Device: '+userAgent+' ---> Error: '+error.message+'\n' )
  }
}

