import { Body, Controller, Get, Post, Put, Query, Req, Res, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { UuidDto } from 'src/dto';
import { createHomeDto } from './dto';
import { deleteHomeDto } from './dto/deleteHomeDto';
import { updateHomeDto } from './dto/updateHomeDto';
import { MyhomeService } from './myhome.service';

@Controller('myhome')
@ApiTags('MyHome')
export class MyhomeController {
    constructor(private service: MyhomeService){}

    @Post('createHome')
    async myhomeCreate(@Body() body:createHomeDto, @Res() res: Response, @Req() req: Request): Promise<any>{
        return await this.service.createHome(body, res, req)
    }

    @Get('getHome/:uuid')
    async getHome(@Query() query:UuidDto, @Res() res:Response, @Req() req: Request ){
        return await this.service.getHome(query, res, req)
    }

    @Put('updateHome')
    async updateHome(@Body() body:updateHomeDto, @Res() res: Response, @Req() req:Request){
        return await this.service.updateHome(body,res,req)
    }

    @Delete('deleteHome')
    async deleteHome(@Body() body:deleteHomeDto, @Res() res: Response, @Req() req:Request){
        return await this.service.deleteHome(body,res,req)
    }
}
