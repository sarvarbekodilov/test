import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as crypto from 'crypto';
import { Request, Response } from 'express';
import Redis  from 'ioredis';
import { AppService } from 'src/app.service';
import { UuidDto } from 'src/dto';
import { createHomeDto } from './dto';
import { deleteHomeDto } from './dto/deleteHomeDto';
import { updateHomeDto } from './dto/updateHomeDto';
const redis = new Redis()

@Injectable()
export class MyhomeService {
    constructor(
        private config: ConfigService,
        private http: HttpService,
        private logs: AppService
    ){}

    async createHome (body: createHomeDto, res: Response, req: Request):Promise<any> {
        let request_id:any
        try {   
            const { uuid, name } = body
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id = new Date().getTime() + uuid

            let sendParams = {
                uuid, name
            };

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }
            let response = await this.httpRequest('createHome', 'POST', sendParams, serHeaders, req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req, error, request_id)
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
    }

    async getHome(query: UuidDto, res: Response, req: Request){
        let request_id:any
        try {
            const { uuid } = query
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id =  new Date().getTime() + uuid

            let sendParams  = {
                uuid
            };

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }
            let response = await this.httpRequest('getHome/'+uuid, 'GET', sendParams, serHeaders, req)
            return res.status(response.data.status).json(response.data)
        } catch (error) {
            this.logs.errorlog(req, error, request_id)
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
    }

    async updateHome(body:updateHomeDto, res: Response, req: Request){
        let request_id:any
        try {
            const { uuid, homeId, name } = body
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id =  new Date().getTime() + uuid

            let sendParams = {
                uuid, homeId, name
            };

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }
            let response = await this.httpRequest('updateHome', 'PUT', sendParams, serHeaders, req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req, error, request_id)
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
    }
    
    async deleteHome (body:deleteHomeDto, res:Response, req:Request):Promise<any> {
        let request_id:any
        try {
            const { uuid, homeId } = body
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id =  new Date().getTime() + uuid

            let sendParams = {
                uuid, homeId
            };

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }
            let response = await this.httpRequest('deleteHome', 'DELETE', sendParams, serHeaders, req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req, error, request_id)
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
    }

    async httpRequest(PATH: string, METHOD:any, data:any, setheaders:any, req:Request): Promise<any> {
        let headerData = {
            'x-model': setheaders.device,
            'authorization': setheaders.token,
            'ip_adress': setheaders.ip_adress,
            'x-request-id': setheaders?.request_id,
            'sign': setheaders.sign, 
            'lang': setheaders?.lang || 'ru',
            'Content-Type': 'application/json',
        };  
        
        let responseData: any
        try {
            let respData:any
            if (METHOD == 'GET') {
                respData = await this.http.get(this.config.get('MY_HOME')+PATH, {headers: headerData}).toPromise();
            } else if (METHOD == 'POST'){
                respData = await this.http.post(this.config.get('MY_HOME')+PATH, data, {headers: headerData}).toPromise();
            } else if (METHOD == 'PUT'){
                respData = await this.http.put(this.config.get('MY_HOME')+PATH, data, {headers: headerData}).toPromise();
            } else if (METHOD == 'DELETE'){
                respData = await this.http.delete(this.config.get('MY_HOME')+PATH, {data, headers: headerData}).toPromise();
            }

            responseData = { data: {status: respData.status, data: respData.data}, responseHeader: respData.headers }    
            let is_valid = await this.rsaVerify(respData.data?.data, respData.headers?.sign)        
            if(!is_valid) throw new Error('Internal server error occured!')          
        } catch (e) {
            responseData = { data: {status: e.response.status, data: e.response.data}, responseHeader: e.response.headers }
        }
        this.logs.log(req, setheaders.request_id, responseData)
		return responseData
    }  

    rsaSign (data:any, privateKey:string):string{
        const signature = crypto.sign("sha256", Buffer.from(JSON.stringify(data)), {
            key: privateKey
        });
        
        return signature.toString('base64')
    }

    rsaVerify(data:any, sign:string):Boolean{
        try {
            let publicKey = this.config.get('PUBLIC_KEY')
            
            const isVerified = crypto.verify(
                "SHA256",
                Buffer.from(JSON.stringify(data)),
                {
                    key: publicKey,
                },
                Buffer.from(sign, 'base64')
            );
            return isVerified
        } catch (error) {
            console.log(error)
            return false
        }
    }
}
