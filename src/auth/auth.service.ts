import { NEST_PGPROMISE_CONNECTION } from 'nestjs-pgpromise';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Response, Request } from 'express';
import { HttpService } from '@nestjs/axios';

import { IDatabase } from 'pg-promise';
import * as crypto from 'crypto';
import Redis from 'ioredis';
import { AuthDto, passwordVerifyDto, resendDto, smsDataDto } from './dto';
import { AppService } from 'src/app.service';

@Injectable()
export class AuthService {
    constructor(
        private http: HttpService, private config: ConfigService,
        @Inject(NEST_PGPROMISE_CONNECTION) private pg: IDatabase<any>,
        private logs: AppService
    ){}

   async login (loginData:AuthDto, res:Response, req:Request):Promise<any> {
        let request_id:any
		try {
            const { phone } = loginData
            const device = req.headers['user-agent']
            const lang = req.headers['Accept-Language']
            const ip_adress = req.ip
            const redis = new Redis();
            request_id = new Date() + phone
            let user:Array<{}> = await this.pg.query('select * from users where phone = $1', [phone])

            if (!user.length) {
                return res.status(403).json({
                    "data": {},
                    "action": {},
                    "message": {
                        "error_text": "Сначала зарегистрируйтесь в приложении",
                        "error_code": 403
                    }
                })
            }

            const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
                modulusLength: 512,
                publicKeyEncoding: {
                    type: 'spki',
                    format: 'pem'
                },
                privateKeyEncoding: {
                    type: 'pkcs8',
                    format: 'pem',
                }
            });

            let p_key = publicKey.split('-----BEGIN PUBLIC KEY-----')[1].split('-----END PUBLIC KEY-----').join('')
            let sendParams = {
                phone: phone,
                clientPublic: p_key
            };
            
            let sign = await this.rsaSign(sendParams, privateKey)
            let sendHeaders = {
                sign, uuid: phone, device, ip_adress, lang
            }
            
            let response = await this.httpRequest('signup', sendParams, sendHeaders, req)
            if(response?.data?.data?.action?.step == 'checkpassword') {
                redis.set(response?.data?.data?.data?.uuid, privateKey)
            }   
            
            return res.status(response.data.status).json(response.data)  
        } catch (error) {
            this.logs.errorlog(req, error, request_id)         
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
   }

   async checkPassword (passwordData: passwordVerifyDto, res:Response, req:Request ): Promise<any> {
        let request_id:any
        try {
            const { password, uuid } = passwordData
            const device = req.headers['user-agent']
            const lang = req.headers['Accept-Language']
            const ip_adress = req.ip
            const redis = new Redis();
            request_id = new Date() + uuid
            let sendParams = {
                password,
                uuid
            };
            let privateKey = await redis.get(uuid)
        
            let sign = await this.rsaSign(sendParams, privateKey)     
            let sendHeaders = {
                sign, uuid, device, ip_adress, lang
            } 

            let response = await this.httpRequest('passwordverify', sendParams, sendHeaders, req)
            
            return res.status(response.data.status).json(response.data)  
        } catch (error) {
            this.logs.errorlog(req, error, request_id ) 
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
   }

   async checkSms(smsData: smsDataDto, res:Response, req:Request): Promise<any> {
        let request_id:any
        try {
            const { smsCode, uuid } = smsData
            const device = req.headers['user-agent']
            const lang = req.headers['Accept-Language']
            const ip_adress = req.ip
            const redis = new Redis();
            request_id = new Date() + uuid
            let sendParams = {
                smsCode,
                uuid
            };

            let privateKey = await redis.get(uuid)
            let sign = await this.rsaSign(sendParams, privateKey)

            let sendHeaders = {
                sign, uuid, device, ip_adress, lang
            }
            let response = await this.httpRequest('checksms', sendParams, sendHeaders,req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req, error, request_id ) 
            return res.status(500).json({status: 500, error:"Internal server error"})
        }
   }

   async resendSms(resData:resendDto, res: Response, req:Request): Promise<any>  {
        let request_id:any
        try {
            const { uuid } = resData
            const device = req.headers['user-agent']
            const lang = req.headers['Accept-Language']
            const ip_adress = req.ip
            const redis = new Redis();
            request_id = new Date() + uuid
            let sendParams = {
                uuid
            };

            let privateKey = await redis.get(uuid)
        
            let sign = await this.rsaSign(sendParams, privateKey)      

            let sendHeaders = {
                sign, uuid, device, ip_adress, lang
            }
            let response = await this.httpRequest('resend', sendParams, sendHeaders, req)

            return res.status(response.data.status).json(response.data)  
        } catch (error) {
            this.logs.errorlog(req, error, request_id ) 
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
   }

   async forgetPassword(forgetData: resendDto, res: Response, req:Request): Promise<any> {
    let request_id:any
    try {
        const { uuid } = forgetData
        const lang = req.headers['Accept-Language']
        const device = req.headers['user-agent']
        const ip_adress = req.ip
        const redis = new Redis();
        request_id = new Date() + uuid
        let sendParams = {
            uuid
        };
        let privateKey = await redis.get(uuid)
    
        let sign = await this.rsaSign(sendParams, privateKey)  
        let sendHeaders = {
            sign, uuid, device, ip_adress, lang
        }

        let response = await this.httpRequest('forgetpassword', sendParams, sendHeaders, req)

        return res.status(response.data.status).json(response.data)  
    } catch (error) {
        this.logs.errorlog(req, error, request_id) 
        return res.status(500).json({
            "data": {},
            "action": {},
            "message": {
                "error_text": "Internal server error occured!",
                "error_code": 500
            }
        })
    }
   }

   async httpRequest(path: string, data:any, setheaders:any, req: Request): Promise<any> {
        let headerData = {
            'x-model': setheaders.device,
            'ip_adress': setheaders.ip_adress,
            'x-request-id': new Date().getTime() + setheaders?.uuid,
            'sign': setheaders.sign, 
            'lang': setheaders?.lang || 'ru',
            'Content-Type': 'application/json',
        };
        
        let responseData: any
        try {
            const respData = await this.http.post(this.config.get('AUTH_URL')+path, data, {headers: headerData}).toPromise();
            responseData = { data: {status: respData.status, data: respData.data}, responseHeader: respData.headers }
            let is_valid = await this.rsaVerify(respData.data?.data, respData.headers?.sign)        
            if(!is_valid) throw new Error('Internal server error occured!')  
        } catch (e) {
            responseData = { data: {status: e.response.status, data: e.response.data}, responseHeader: e.response.headers }
        }
        this.logs.log(req, setheaders.request_id, responseData)
		return responseData
   }    

   rsaSign (data:any, privateKey:string):string{
        const signature = crypto.sign("sha256", Buffer.from(JSON.stringify(data)), {
            key: privateKey
        });
        
        return signature.toString('base64')
   }

   rsaVerify(data:any, sign:string):Boolean{
        try {
            let publicKey = this.config.get('PUBLIC_KEY')
            
            const isVerified = crypto.verify(
                "SHA256",
                Buffer.from(JSON.stringify(data)),
                {
                    key: publicKey,
                },
                Buffer.from(sign, 'base64')
            );
            return isVerified
        } catch (error) {
            console.log(error)
            return false
        }
   }

}
