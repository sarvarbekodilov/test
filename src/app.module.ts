import { Module } from '@nestjs/common';
import { ConfigModule,ConfigService } from '@nestjs/config';
import { NestPgpromiseModule } from 'nestjs-pgpromise';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CardsModule } from './cards/cards.module';
import { WsModule } from './ws/ws.module';
import { PayModule } from './pay/pay.module';
import { MyhomeModule } from './myhome/myhome.module';


let config = new ConfigService()

@Module({
  imports: [
    ConfigModule.forRoot({isGlobal:true}),
    NestPgpromiseModule.register({
      connection: {
          host: config.get('DATABASE_URL'),
          port: config.get('PORT'),
          database: config.get('DATABSE'),
          user: config.get('USER'),
          password: config.get('PASSWORD'),
      },
  }),
    AuthModule,
    CardsModule,
    WsModule,
    PayModule,
    MyhomeModule],
  controllers: [],
  providers: [AppService],
  exports: [AppService]
})
export class AppModule {}
