import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { AppService } from 'src/app.service';
import { WsModule } from 'src/ws/ws.module';
import { PayController } from './pay.controller';
import { PayService } from './pay.service';

@Module({
  imports: [WsModule,HttpModule],
  controllers: [PayController],
  providers: [PayService, AppService]
})
export class PayModule {}
