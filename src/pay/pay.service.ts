import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as crypto from 'crypto';
import { UuidDto } from 'src/dto';
import { Request, Response } from 'express';
import Redis  from 'ioredis';
import { panetInfoDto, ServiceListDto } from './dto';
import { AppService } from 'src/app.service';
const redis = new Redis()

@Injectable()
export class PayService {
    constructor(
        private http: HttpService,
        private config: ConfigService,
        private logs: AppService
    ){}

    async categoryList(body: UuidDto, res: Response, req: Request): Promise<any>{
        let request_id:any
        try {
            const { uuid } = body
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id = new Date().getTime() + uuid

            let sendParams = {
                uuid
            };

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }
            let response = await this.httpRequest('categorylist', sendParams, serHeaders, req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req,error, request_id)
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
    }

    async serviceList (body: ServiceListDto, res: Response, req: Request ):Promise<any>{
        let request_id:any
        try {
            const { uuid, category_id } = body
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id = new Date().getTime()+uuid

            let sendParams = {
                uuid,
                category_id
            };

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }
            let response = await this.httpRequest('servicelist', sendParams, serHeaders, req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req, error, request_id )
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
        
    }

    async paynetInfo (body: panetInfoDto, res: Response, req: Request ):Promise<any>{
        let request_id:any
        try {
            const { uuid, service_id, fields, serviceTitle } = body
            const device = req.headers['user-agent']
            const lang = req.headers['accept-language']
            const token = req.headers['authorization']
            const ip_adress = req.ip
            request_id = new Date().getTime()+ uuid

            let sendParams = {
                uuid,
                service_id,
                serviceTitle,
                fields
            }
            

            let privateKey = await redis.get(uuid)
            if(!privateKey) throw new Error("Rsa key not found");
            let sign = await this.rsaSign(sendParams, privateKey)      

            let serHeaders = {
                sign, uuid, device, ip_adress, lang, token, request_id
            }

            let response = await this.httpRequest('paynetinfo', sendParams, serHeaders,req)

            return res.status(response.data.status).json(response.data) 
        } catch (error) {
            this.logs.errorlog(req, error, request_id)
            return res.status(500).json({
                "data": {},
                "action": {},
                "message": {
                    "error_text": "Internal server error occured!",
                    "error_code": 500
                }
            })
        }
        
    }

    async httpRequest(PATH: string, data:any, setheaders:any, req:Request): Promise<any> {
        let headerData = {
            'x-model': setheaders.device,
            'authorization': setheaders.token,
            'ip_adress': setheaders.ip_adress,
            'x-request-id': new Date().getTime() + setheaders?.uuid,
            'sign': setheaders.sign, 
            'lang': setheaders?.lang || 'ru',
            'Content-Type': 'application/json',
        };
        
        let responseData: any
        try {
            const respData = await this.http.post(this.config.get('PAY_URL')+PATH, data, {headers: headerData}).toPromise();
            responseData = { data: {status: respData.status, data: respData.data}, responseHeader: respData.headers }    
            let is_valid = await this.rsaVerify(respData.data?.data, respData.headers?.sign)        
            if(!is_valid) throw new Error('Internal server error occured!')          
        } catch (e) {
            responseData = { data: {status: e.response.status, data: e.response.data}, responseHeader: e.response.headers }
        }
        this.logs.log(req, setheaders.request_id, responseData)
		return responseData
    }  

    rsaSign (data:any, privateKey:string):string{
        const signature = crypto.sign("sha256", Buffer.from(JSON.stringify(data)), {
            key: privateKey
        });
        
        return signature.toString('base64')
    }

    rsaVerify(data:any, sign:string):Boolean{
        try {
            let publicKey = this.config.get('PUBLIC_KEY')
            
            const isVerified = crypto.verify(
                "SHA256",
                Buffer.from(JSON.stringify(data)),
                {
                    key: publicKey,
                },
                Buffer.from(sign, 'base64')
            );
            return isVerified
        } catch (error) {
            console.log(error)
            return false
        }
    }
}
