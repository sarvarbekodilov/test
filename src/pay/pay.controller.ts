import { Body, Controller, Post, Req, Res } from '@nestjs/common';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { resendDto } from 'src/auth/dto';
import { UuidDto } from 'src/dto';
import { panetInfoDto, ServiceListDto } from './dto';
import { PayService } from './pay.service';

@Controller('pay')
@ApiTags('pay')
export class PayController {
    constructor(private service: PayService) {}

    @Post('categorylist')
    @ApiHeader({ name: 'Accept-Language', description: 'Default ru' })
    async categoryList(@Body() body: UuidDto, @Res() res: Response, @Req() req: Request) {
        return this.service.categoryList(body, res, req)
    }

    @Post('servicelist')
    @ApiHeader({ name: 'Accept-Language', description: 'Default ru' })
    async servicelist(@Body() body: ServiceListDto, @Res() res: Response, @Req() req: Request) {
        return this.service.serviceList(body, res, req)
    }

    @Post('paynetinfo')
    @ApiHeader({ name: 'Accept-Language', description: 'Default ru' })
    async paynetInfo(@Body() body: panetInfoDto, @Res() res: Response, @Req() req: Request) {
        return this.service.paynetInfo(body, res, req)
    }

    
}
