import { IsObject, IsString } from 'class-validator'
import { ApiProperty } from "@nestjs/swagger"; 

export class panetInfoDto {
    @IsString() 
    @ApiProperty({type: String, default: '99eae575-12ae-48bb-8470-fd9e214b004e', description: 'uuid'})
    uuid: string
    
    @IsString() 
    @ApiProperty({type: String, default: "3513", description: 'Service id'})
    service_id: string

    @IsObject() 
    @ApiProperty({type: Object,  default: {"clientid":"902060398"} ,description: 'Client id'})
    fields: object

    @IsString() 
    @ApiProperty({type: String, default:"Номер счёта", description: 'Service title'})
    serviceTitle: string
}